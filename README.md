# Remote Monitoring and Teleoperation of AVs Data

This repository contains the experimental data collected for the paper: Remote Monitoring and Teleoperation of Autonomous Vehicles —Is Virtual Reality an Option?
Each folder in the zipped folder contains a readme file specific to the study, make sure to go through it for more information about the data.

## Data Usage
This data can be used for various analyses and research related to the Lockhart and Stroke Error tests. If you use this data, please ensure proper attribution and adhere to any licensing or usage restrictions.

## License

Please specify any applicable license for your data, such as a Creative Commons license or any other terms under which the data can be used.

## OSF Link
You can access the data and related resources on the Open Science Framework (OSF) platform via the following link: [OSF Link](https://osf.io/s6bu5/).

## Contributing
If you wish to contribute to this dataset or report issues, please follow the GitHub standard procedures for contributing to repositories.

## Acknowledgments

If there are any specific acknowledgments or references related to the data, please include them here.
For questions or clarifications, contact [snehanjali.kalamkar@hs-coburg.de].
Thank you for using this dataset!
